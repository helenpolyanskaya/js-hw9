const earth = ["Africa", "North America", "South America", "Antarctica", "Eurasia", "Australia"];

function getList(a, b){
    let div = document.createElement("div");
    b.append(div);
    let ol = document.createElement("ol");
    div.append(ol);
    let fragment = new DocumentFragment();
    for (let key of a){
    let li = document.createElement("li");
    li.append(key);
    fragment.append(li);       
    }
    return ol.append(fragment);
}

getList(earth, document.body);

const earth2 = ["Africa", "North America", "South America", "Antarctica", ["Asia", "Europa"], "Australia"];

function getList2(a, b){
    let div = document.createElement("div");
    b.append(div);
    let ul = document.createElement("ul");
    div.append(ul);
    let fragment = new DocumentFragment();
    for (let key of a){
        let li = document.createElement("li");
            if ( Array.isArray(key) ) {
                getList2(key, li);
            } else {
            li.append(key);
            }
        fragment.append(li);       
    }
    return ul.append(fragment);
}

getList2(earth2, document.body);

let div2 =  document.createElement("div");
div2.className = "div";
div2.innerHTML = "<strong>03<strong>";
document.body.append(div2);
setTimeout(() => div2.remove(), 1000);
let div3 =  document.createElement("div");
div3.className = "div";
div3.innerHTML = "<strong>02<strong>";
setTimeout(() => document.body.append(div3), 1000)
setTimeout(() => div3.remove(), 2000);
let div4 =  document.createElement("div");
div4.className = "div";
div4.innerHTML = "<strong>01<strong>";
setTimeout(() => document.body.append(div4), 2000)
setTimeout(() => div4.remove(), 3000);

setTimeout(() => document.body.innerHTML = "", 3000);
